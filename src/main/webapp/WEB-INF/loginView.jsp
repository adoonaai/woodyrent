<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>

<div class="wrapper">
    <div class="container">
        <h3 class="title">Login Page</h3>

        <p class="error">${errorString}</p>

        <form method="POST" action="${pageContext.request.contextPath}/login">
            <input type="hidden" name="redirectId" value="${param.redirectId}" />
            <table border="0">
                <tr>
                    <td>User Name</td>
                    <td>
                        <input class="input" type="text" name="login" value= "${user.login}" />
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input class="input" type="password" name="password" value= "${user.password}" />
                    </td>
                </tr>

                <tr>
                    <td colspan ="2" class="buttons-container">
                        <div class="buttons">
                            <a href="${pageContext.request.contextPath}/" class="cancel">Выход</a>
                            <input type="submit" value= "Вход" class="submit"/>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

</body>
</html>