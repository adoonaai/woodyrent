package com.realization.filter;

import com.realization.bean.Role;
import com.realization.bean.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/managerTask/*")
public class RoleFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        User user = (User) httpRequest.getSession().getAttribute("user");
        if (user != null && user.getRole().equals(Role.ADMIN)){
            filterChain.doFilter(servletRequest,servletResponse);
        }else {
            httpResponse.sendRedirect("/WEB-INF/views/accessDeniedView.jsp");
        }
    }

    @Override
    public void destroy() {

    }


}
