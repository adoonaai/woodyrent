package com.realization.servlet;


import com.realization.bean.User;
import com.realization.config.TokenGenerator;
//import com.realization.utils.DataDAO;

import com.realization.service.UserService;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {

//    private final UserDao userDao = ContextLoaderListener.getCurrentWebApplicationContext().getBean(UserDao.class);
    private final UserService userService = ContextLoaderListener.getCurrentWebApplicationContext().getBean(UserService.class);
    private final TokenGenerator tokenGenerator = ContextLoaderListener.getCurrentWebApplicationContext().getBean(TokenGenerator.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("2222222");

        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");

        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("1111");

        String userName = request.getParameter("login");
        String password = request.getParameter("password");

        User user = userService.findByLogin(userName,password);

        if (user != null) {

            String token = tokenGenerator.getToken(user.getId(), user.getLogin(), user.getRole());
            HttpSession session = request.getSession();
            session.setAttribute("userJwt", token);

            response.sendRedirect(request.getContextPath() + "/main.jsp");

            System.out.println("я вот здесь юзер есть");
        }else {

            System.out.println("я вот здесь юзера нет");
            String errorMessage = "Invalid userName or Password";

            request.setAttribute("errorMessage", errorMessage);
            request.getRequestDispatcher("/loginView.jsp").forward(request,response);

        }
    }
}



//
//            request.getSession().setAttribute("user", userAccount);
//        if (userName.equals(userAccount.getUserName()) && password.equals(userAccount.getPassword()) && TypeUser.ADMIN.equals((userAccount.getTypeUser()))) {
//
//            request.getRequestDispatcher("/WEB-INF/views/managerTaskView.jsp").forward(request, response);
//        } else if (userName.equals(userAccount.getUserName()) && password.equals(userAccount.getPassword())) {
//            session.setAttribute("userJwt",token);
//            request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp").forward(request, response);
//        } else {
//            request.setAttribute("errorMessage", errorMessage);
////            request.getRequestDispatcher("homeView.jsp").forward(request, response);

