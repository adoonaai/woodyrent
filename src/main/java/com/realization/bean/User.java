package com.realization.bean;

public class User {

    private Long id;
    private String login;
    private double balance;
    private String password;
    private Role role;


    public User() {
    }

    public User(Long id, String login, double balance, String password, Role role) {
        this.id = id;
        this.login = login;
        this.balance = balance;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}