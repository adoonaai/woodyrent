package com.realization.config;

import com.realization.bean.Role;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class TokenGenerator {

    public String getToken(Long id, String login, Role role){
        Map<String,Object> claims = new HashMap<>();
        claims.put("id",id);
        claims.put("login",login);
        claims.put("role", role);
        Date now = new Date();
        Date expirationData = new Date(now.getTime()+60*60*1000);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(String.valueOf(id))
                .setIssuedAt(now)
                .setExpiration(expirationData)
                .signWith(SignatureAlgorithm.HS512,"secret").compact();


    }
}
