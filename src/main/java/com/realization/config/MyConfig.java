package com.realization.config;


import com.realization.service.UserService;
import com.realization.service.UserServiceImpl;
import com.realization.utils.DBConnectionManager;
import com.realization.utils.UserDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@Configuration
//@ComponentScan("com.realization")
public class MyConfig {

    @Bean
    public UserDao userDao(){
        return new UserDao(dbConnectionManager());
    }

    @Bean
    public DBConnectionManager dbConnectionManager(){
        return new DBConnectionManager();
    }

    @Bean
    public TokenGenerator createToken(){
        return new TokenGenerator();
    }

    @Bean
    public UserService userService(){
        return new UserServiceImpl();
    }


//    @Bean
//    public LoginServlet loginServlet(){
//        System.out.println("3");
//        return new LoginServlet(userDao());
//    }



}
