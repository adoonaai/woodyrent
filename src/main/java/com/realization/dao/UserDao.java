package com.realization.dao;

import com.realization.TypeOfUsers;
import com.realization.Users;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDao {

    private List<Users> users = new ArrayList<>();

    public static void main(String[] args) {

        UserDao userDao = new UserDao();
        System.out.println("Регистрация пользователя");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите логин");
        String login = scanner.nextLine();
        System.out.println("Введите пароль");
        String password = scanner.nextLine();

        if (dataChecking(login, password, password)) {
            userDao.create(login, password, password);
        }
        System.out.println("Вход в систему");
        System.out.println("Введите логин для входа");
        String joinLogin = scanner.nextLine();
        System.out.println("Введите пароль для входа");
        String joinPassword = scanner.nextLine();
        userDao.join(joinLogin, joinPassword);

    }

    public List<Users> create(String login, String password, String confirmPassword) {
        users.add(new Users(UUID.randomUUID(), login, password, confirmPassword, TypeOfUsers.USERS));
        System.out.println("Пользователь создан");
        return users;
    }

    public static boolean dataChecking(String login, String password, String confirmPassword) {
        Pattern pattern = Pattern.compile("\\w{5,20}([a-zA-Z0-9_])");
        Matcher matcherLogin = pattern.matcher(login);
        Matcher matcherPassword = pattern.matcher(password);
        boolean foundLogin = matcherLogin.matches();
        try {
            if (foundLogin) {
                System.out.println("Логин удовлетворяет условия");
            } else {
                throw new Exception("Логин не удовлетворяет условию");
            }
            boolean foundPassword = matcherPassword.matches();
            if (foundPassword && (password.equals(confirmPassword))) {
                System.out.println("Пароль удовлетворяет условию");
            } else {
                throw new Exception("Пароль не удовлетворяет условию");
            }
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public void join(String login, String password) {

        for (Users buf : users) {
            if (buf.getLogin().equals(login) && buf.getPassword().equals(password)) {
                System.out.println("Вход в систему");
            } else {
                System.out.println("Такого пользователя нет");
            }
        }
    }
}
