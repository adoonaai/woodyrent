package com.realization.dao;

import com.realization.*;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VehicleDao {

    private List<Bike> bike;
    private List<KickScooter> kickScooters;

    public static void main(String[] args) {

        VehicleDao vehicleDao = new VehicleDao();

        vehicleDao.addBike();
        vehicleDao.addKickScooter();
        vehicleDao.showVehicle(vehicleDao.index(TypeOfTransport.KICK_SCOOTER));
        vehicleDao.showVehicle(vehicleDao.index(TypeOfTransport.BIKE));
    }

    public List<Bike> addBike() {
        bike = new ArrayList<>();
        for(int i =0; i< 5; i++) {
            bike.add(new Bike(UUID.randomUUID(), TypeOfTransport.BIKE, Condition.EXCELLENT, "Model 1", 3.4, "0.0 0.0"));
            i++;
        }
        return bike;
    }

    public List<KickScooter> addKickScooter() {
        kickScooters = new ArrayList<>();
        for(int i =0; i< 5; i++) {
            kickScooters.add(new KickScooter(UUID.randomUUID(), TypeOfTransport.KICK_SCOOTER, Condition.EXCELLENT, "Model 2", 4.6, "0.0 0.0", 15, 100));
        }
        return kickScooters;
    }

    public List<? extends Vehicle> index(TypeOfTransport typeOfTransport) {

        switch (typeOfTransport) {
            case BIKE:
                return bike;
            case KICK_SCOOTER:
                return kickScooters;
        }
        return null;
    }

    public void showVehicle(List<? extends Vehicle> vehicles) {
        for (Vehicle vehicle : vehicles) {
            System.out.println(vehicle.getCondition());
            System.out.println(vehicle.getId());
            System.out.println(vehicle.getRentPrice());
            System.out.println(vehicle.getTypeOfTransport());
            System.out.println(vehicle.getGpsCoordinates());
            System.out.println(vehicle.getModel());
        }
    }

}
