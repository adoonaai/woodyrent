package com.realization.utils;

import com.realization.bean.Role;
import com.realization.bean.User;

import java.sql.*;


public class UserDao{

    private final DBConnectionManager dbConnectionManager;

    public UserDao(DBConnectionManager dbConnectionManager) {
        System.out.println("333333333");
        this.dbConnectionManager = dbConnectionManager;
    }

    //
//    private static final String URL = "jdbc:postgresql://localhost:5432/woodyrent";
//    private static final String USERNAME = "postgres";
//    private static final String PASSWORD = "portrt89";
//
//    private static Connection connection;
//
//    static {
//        try {
//            Class.forName("org.postgresql.Driver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//
//        try {
//            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//    }

    public User findUser(String username) {

        System.out.println("44444444444444");

        User user = null;
        try {
            PreparedStatement preparedStatement =
                    dbConnectionManager.connection().prepareStatement("SELECT * FROM client WHERE login=?");
            preparedStatement.setString(1, username);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                user = new User();

                user.setId(resultSet.getLong("id"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setBalance(resultSet.getInt("balance"));
                user.setRole(Role.valueOf(resultSet.getString("role")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }
}

