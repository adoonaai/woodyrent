package com.realization.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.SQLException;


@PropertySource("classpath:config.properties")
public class DBConnectionManager {

//    @Value("${db.url}")
    private static String URL ="jdbc:postgresql://localhost:5432/woodyrent";



//    @Value("${db.username}")
    private static String USERNAME = "postgres";

//    @Value("${db.password}")
    private static String PASSWORD = "portrt89";

    public Connection connection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

}