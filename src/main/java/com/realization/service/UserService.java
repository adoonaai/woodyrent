package com.realization.service;

import com.realization.bean.User;


public interface UserService {

    User findByLogin(String login, String password);
}
