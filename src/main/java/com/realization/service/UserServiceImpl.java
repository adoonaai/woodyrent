package com.realization.service;

import com.realization.bean.User;
import com.realization.utils.UserDao;
import org.springframework.beans.factory.annotation.Autowired;


public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;

    public UserServiceImpl() {

    }

    @Override
    public User findByLogin(String login, String password) {

        User user = userDao.findUser(login);
        if (user.getPassword().equals(password)){
            return user;
        }else {
            return null;
        }

    }
}
